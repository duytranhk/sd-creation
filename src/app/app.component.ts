import { Component } from '@angular/core';
import { LoaderService } from './services/loader.service';
import { AuthService } from './services/auth.service';
import { UserModel } from './models/auth.model';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  constructor(private loaderService: LoaderService, private authService: AuthService) {
  }

  public get isLoading(): boolean {
    return this.loaderService.isLoading;
  }

  public get isLoggedIn(): boolean {
    return this.authService.IsUserLoggedIn;
  }

  public get currentUser(): UserModel {
    return this.authService.currentUser;
  }

  public onLogOutClick(): void {
    const self = this;
    self.authService.logout();
  }
}

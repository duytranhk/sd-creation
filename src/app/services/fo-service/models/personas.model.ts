

export interface MemberPersonas {
    Id: number;
    Age: number;
    Avatar: string;
    Gender: string;
    HouseholdId: number;
    Name: string;
    Relationship: string;
    ResidencyStatus: string;
}

export interface Personas {
    Id: number;
    Age: number;
    Avatar: number;
    CurrencyCode: string;
    Gender: string;
    HouseholdId: number;
    Members: MemberPersonas[];
    Name: string;
    Occupation: string;
    PersonaId: number;
    PersonaName: string;
    Profession: string;
    Relationship: string;
    ResidencyStatus: string;
    TotalIncome: number;
}

export interface PersonasDuplicateResult {
    CustomerId: number;
    FirstName: string;
    HouseholdId: number;
    LastName: string;
    PlanId: number;
}

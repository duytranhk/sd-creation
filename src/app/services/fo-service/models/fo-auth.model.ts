export interface FoAuthModel {
    access_token: string;
    expires_in: string;
    token_type: string;
}

import { NgModule } from '@angular/core';
import { ApiEndpointService } from '../api-endpoint.service';
import { FoAuthService } from './fo-auth.service';
import { FOPersonasService } from './fo-personas.service';

@NgModule({
    providers: [ApiEndpointService, FoAuthService, FOPersonasService],
})
export class FoApiServiceModule { }

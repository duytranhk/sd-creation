import { Injectable } from '@angular/core';
import { StorageManagerService } from '../storage-manager.service';
import { ApiEndpointService } from '../api-endpoint.service';
import { FoAuthModel } from './models/fo-auth.model';
import { map } from 'rxjs/operators';
import { ApiMethod } from '../../constants/api-method.const';
import { Observable } from 'rxjs';
import { StorageKey } from '../../constants/storage-key.const';

@Injectable()
export class FoAuthService {
    constructor(
        private apiEndpointService: ApiEndpointService,
        private storageManager: StorageManagerService) {
    }

    public authorize(userName: string, password: string): Observable<boolean> {
        const self = this;
        // end previous session
        self.endSession();
        return self.apiEndpointService.callFoApi<FoAuthModel>(ApiMethod.POST, '/api/Account/auth', {
            UserName: userName,
            Password: password
        }).pipe(map(resp => {
            if (!resp.access_token) { return false; }
            self.storageManager.localStorageSetItem(StorageKey.EVN_ACCESS_TOKEN, resp.access_token);
            self.storageManager.localStorageSetItem(StorageKey.EVN_TOKEN_TYPE, resp.token_type);
            return true;
        }));
    }

    public endSession(): void {
        const self = this;
        self.storageManager.localStorageRemoveItem(StorageKey.EVN_ACCESS_TOKEN);
        self.storageManager.localStorageRemoveItem(StorageKey.EVN_TOKEN_TYPE);

    }
}

import { Injectable } from '@angular/core';
import { ApiEndpointService } from '../api-endpoint.service';
import { IApiResult } from 'src/app/models/common.model';
import { ApiMethod } from 'src/app/constants/api-method.const';
import { Personas } from './models/personas.model';
import { Observable } from 'rxjs';

@Injectable()
export class FOPersonasService {
    constructor(
        private apiEndpointService: ApiEndpointService) {
    }

    public getPersonas(): Observable<IApiResult<{ Personas: Personas[] }>> {
        const self = this;
        return self.apiEndpointService
            .callFoApi<IApiResult<{ Personas: Personas[] }>>(ApiMethod.GET, '/api/personas/GetAgentPersonas');
    }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { StorageManagerService } from './storage-manager.service';
import { StorageKey } from '../constants/storage-key.const';
import { catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { ApiMethod } from '../constants/api-method.const';
import { AlertService } from './alert.service';

@Injectable()
export class ApiEndpointService {
    constructor(private http: HttpClient, private storageManager: StorageManagerService, private alertService: AlertService) {
    }
    public callFoApi<T>(method: ApiMethod, url: string, data?: any): Observable<T> {
        const self = this;
        const tokenType = self.storageManager.localStorageGetItem(StorageKey.EVN_TOKEN_TYPE);
        const token = self.storageManager.localStorageGetItem(StorageKey.EVN_ACCESS_TOKEN);
        const fullUrl = self.storageManager.localStorageGetItem(StorageKey.EVN_API_URL) + url;
        return self.http.request<T>(method as string, fullUrl, {
            body: data,
            headers: token && tokenType ? new HttpHeaders({
                Authorization: `${tokenType} ${token}`
            }) : null
        }).pipe<T>(
            catchError(error => this.handleError<T>(error, () => this.callFoApi<T>(method, url, data)))
        );
    }

    public callAzureAdApi<T>(method: ApiMethod, url: string, data?: any ): Observable<T> {
        const self = this;
        const tokenType = self.storageManager.localStorageGetItem(StorageKey.USER_TOKEN_TYPE);
        const token = self.storageManager.localStorageGetItem(StorageKey.USER_ACCESS_TOKEN);
        return self.http.request<T>(method as string, url, {
            body: data,
            headers: token && tokenType ? new HttpHeaders({
                Authorization: `${tokenType} ${token}`
            }) : null
        }).pipe<T>(
            catchError(error => this.handleError<T>(error, () => this.callFoApi<T>(method, url, data)))
        );
    }

    protected handleError<T>(error, continuation: () => Observable<T>): Observable<T> {
        const self = this;
        if (error.error) {
            self.alertService.show(error.error.ErrorMessage);
        }
        return throwError(error);
    }
}

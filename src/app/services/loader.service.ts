import { Injectable } from '@angular/core';
import { BlockUI, NgBlockUI } from 'ng-block-ui';

@Injectable()
export class LoaderService {
    constructor() { }
    @BlockUI() blockUI: NgBlockUI;

    public isLoading: boolean;

    public on(): void {
        const self = this;
        self.blockUI.start();
        self.isLoading = true;
    }

    public off(): void {
        const self = this;
        self.blockUI.stop();
        self.isLoading = false;
    }
}

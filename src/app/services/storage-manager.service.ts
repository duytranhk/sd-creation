import { Injectable } from '@angular/core';
import { UtilService } from './utilities.service';

@Injectable()
export class StorageManagerService {
  public clearAllStorage() {
    localStorage.clear();
  }

  public localStorageRemoveItem(key: string): void {
    localStorage.removeItem(key);
  }

  public exists(key: string) {
    return !!localStorage.getItem(key);
  }

  public localStorageSetItem(key: string, data: any) {
    localStorage.setItem(key, JSON.stringify(data));
  }

  public localStorageGetItem(key: string) {
    return UtilService.JSonTryParse(localStorage.getItem(key));
  }
}

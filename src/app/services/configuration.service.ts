import { Injectable } from '@angular/core';

@Injectable()
export class ConfigurationService {
    public static readonly appVersion: string = '1.0.0';
    public fallbackBaseUrl = 'http://localhost:50064/';
}

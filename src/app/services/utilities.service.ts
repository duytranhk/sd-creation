import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


@Injectable()
export class UtilService {

  public static readonly captionAndMessageSeparator = ':';
  public static readonly noNetworkMessageCaption = 'No Network';
  public static readonly noNetworkMessageDetail = 'The server cannot be reached';
  public static readonly accessDeniedMessageCaption = 'Access Denied!';
  public static readonly accessDeniedMessageDetail = '';


  public static JSonTryParse(value: string) {
    try {
      return JSON.parse(value);
    } catch (e) {
      if (value === 'undefined') {
        return void 0;
      }

      return value;
    }
  }

  public static isObjectEmpty(obj: any) {
    for (const prop in obj) {
      if (obj.hasOwnProperty(prop)) {
        return false;
      }
    }

    return true;
  }

  public static isUndefined(value: any) {
    return typeof value === 'undefined';
  }

  public static uniqueId() {
    return this.randomNumber(1000000, 9000000).toString();
  }

  public static randomNumber(min: number, max: number) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  public static createObservable<T>(data: T): Observable<T> {
    return Observable.create(() => data);
  }


  public static urlBase64Decode(str: string) {
    let output = str.replace(/-/g, '+').replace(/_/g, '/');
    switch (output.length % 4) {
      case 0: { break; }
      case 2: { output += '=='; break; }
      case 3: { output += '='; break; }
      default: {
        throw new Error('Illegal base64url string!');
      }
    }
    return decodeURIComponent((window as any).escape(window.atob(output)));
  }

  public static decodeToken(token: string = '') {
    if (token === null || token === '') {
      return { upn: '' };
    }
    const parts = token.split('.');

    if (parts.length !== 3) {
      throw new Error('JWT must have 3 parts');
    }

    const decoded = this.urlBase64Decode(parts[1]);
    if (!decoded) {
      throw new Error('Cannot decode the token');
    }

    return this.JSonTryParse(decoded);
  }

  public static parseQueryString(url: string): any {
    const params = {};
    let queryString = '';
    if (url.search('#') !== -1) {
      queryString = url.substring(url.search('#') + 1);

    } else {
      queryString = url.substring(url.indexOf('?') + 1);
    }
    const a = queryString.split('&');

    for (const i of a) {
      const b = i.split('=');
      params[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
    }
    return params;
  }
}

import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { EnvironmentModel, InsitutionModel } from '../models/environment.model';
import { Observable } from 'rxjs';
import { FirebaseDb } from '../constants/firebasedb.const';
import { StorageManagerService } from './storage-manager.service';
import { StorageKey } from '../constants/storage-key.const';

@Injectable()
export class EnvironmentService {
    protected enviroments: AngularFireList<EnvironmentModel>;
    constructor(private db: AngularFireDatabase, private storageManager: StorageManagerService) {
        this.enviroments = this.db.list<EnvironmentModel>(FirebaseDb.Environments);
    }

    public getEnvironments(): Observable<EnvironmentModel[]> {
        const self = this;
        return self.enviroments.valueChanges();
    }

    public setUpEnvironment(env: EnvironmentModel, institution: InsitutionModel): void {
        const self = this;
        if (env && institution) {
            self.storageManager.localStorageSetItem(StorageKey.EVN_API_URL, env.BaseUrl);
        }
    }
}

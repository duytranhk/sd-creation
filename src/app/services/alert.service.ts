import { MatSnackBar } from '@angular/material/snack-bar';
import { Injectable } from '@angular/core';

@Injectable()
export class AlertService {
    constructor(readonly snackBar: MatSnackBar) {
    }

    public show(message: string, delay: number = 3500): void {
        const self = this;
        self.snackBar.open(message, 'dismiss', {
            duration: 3500,
        });
    }
}

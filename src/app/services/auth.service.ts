import { Injectable } from '@angular/core';
import { StorageManagerService } from './storage-manager.service';
import { ApiEndpointService } from './api-endpoint.service';
import { UserModel } from '../models/auth.model';
import { ApiMethod } from '../constants/api-method.const';
import { Observable } from 'rxjs';
import { StorageKey } from '../constants/storage-key.const';
import { Router } from '@angular/router';
import { UserCredential } from 'electron/core/models/credential.model';
import * as MicrosoftGraph from '@microsoft/microsoft-graph-types';

@Injectable()
export class AuthService {
    public currentUser: UserModel;
    constructor(
        private apiEndpointService: ApiEndpointService,
        private storageManager: StorageManagerService,
        private router: Router) {
    }


    public get IsUserLoggedIn(): boolean {
        return !!this.currentUser;
    }

    public processUserLogin(creadential: UserCredential): void {
        const self = this;
        if (creadential) {
            self.storageManager.localStorageSetItem(StorageKey.USER_ACCESS_TOKEN, creadential.AccessToken);
            self.storageManager.localStorageSetItem(StorageKey.USER_TOKEN_TYPE, creadential.TokenType);
            self.storageManager.localStorageSetItem(StorageKey.USER_TOKEN_EXPIRY, creadential.TokenExpiry);
        }
        self.apiEndpointService
            .callAzureAdApi<MicrosoftGraph.User>(ApiMethod.GET, 'https://graph.windows.net/me?api-version=1.6')
            .subscribe(resp => {
                self.currentUser = {
                    Email: resp.mail,
                    Department: resp.department,
                    DisplayName: resp.displayName
                };
                self.router.navigateByUrl('/');
            });
    }

    public logout(): void {
        const self = this;
        self.currentUser = null;
        self.storageManager.clearAllStorage();
        this.router.navigateByUrl('/signin');
    }
}

// module
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { MatDividerModule } from '@angular/material/divider';
import { MatCardModule } from '@angular/material/card';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSelectModule } from '@angular/material/select';
import { MatGridListModule } from '@angular/material/grid-list';
import { FormsModule } from '@angular/forms';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { AppRoutingModule } from './app.routing.module';
import { NgxElectronModule } from 'ngx-electron';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { BlockUIModule } from 'ng-block-ui';

// services
import { UtilService } from './services/utilities.service';
import { StorageManagerService } from './services/storage-manager.service';
import { ApiEndpointService } from './services/api-endpoint.service';
import { AuthService } from './services/auth.service';
import { ConfigurationService } from './services/configuration.service';
import { AlertService } from './services/alert.service';
import { AuthGuardService } from './services/auth-guard.service';


// Components
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { SdUserComponent } from './components/sd-user/sd-user.component';
import { LoginComponent } from './components/login/login.component';

import { environment } from '../environments/environment';
import { EnvironmentService } from './services/environment.service';
import { LoaderService } from './services/loader.service';
import { FoApiServiceModule } from './services/fo-service/fo-service.module';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SdUserComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgxElectronModule,
    MatDividerModule,
    MatCardModule,
    BrowserAnimationsModule,
    MatSelectModule,
    MatGridListModule,
    MatTooltipModule,
    FormsModule,
    MatProgressSpinnerModule,
    MatInputModule,
    MatButtonModule,
    MatSnackBarModule,
    MatIconModule,
    MatCheckboxModule,
    AngularFireModule.initializeApp(environment.firebaseConfig, 'bto-tool'),
    AngularFireDatabaseModule,
    BlockUIModule.forRoot(),
    FoApiServiceModule
  ],
  providers: [
    UtilService,
    StorageManagerService,
    ApiEndpointService,
    AuthService,
    ConfigurationService,
    AlertService,
    AuthGuardService,
    EnvironmentService,
    LoaderService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

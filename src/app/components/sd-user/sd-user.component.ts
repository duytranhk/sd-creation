import { ApplicationConst } from 'src/app/constants/application.const';
import { IApiResult } from 'src/app/models/common.model';
import { ApiEndpointService } from 'src/app/services/api-endpoint.service';
import { ApiMethod } from 'src/app/constants/api-method.const';
import { NgForm } from '@angular/forms';
import { AlertService } from 'src/app/services/alert.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { fadeInOut } from 'src/app/services/animation.service';
import { EnvironmentService } from 'src/app/services/environment.service';
import { EnvironmentModel, InsitutionModel, BranchModel } from 'src/app/models/environment.model';
import { LoaderService } from 'src/app/services/loader.service';
import { FoAuthService } from 'src/app/services/fo-service/fo-auth.service';
import { FOPersonasService as FOPersonaService } from 'src/app/services/fo-service/fo-personas.service';
import { Personas } from 'src/app/services/fo-service/models/personas.model';
import { setLines } from '@angular/material/core';

interface SdCreationModel {
    EnvironmentId?: number;
    InstitutionId?: number;
    BranchId?: number;
    SelectedEnvironment?: EnvironmentModel;
    SelectedInstitution?: InsitutionModel;
    SelectedBranch?: BranchModel;
    CustomAgent?: CustomAgentModel;
    isUseCustomAgent?: boolean;
}

class CustomAgentModel {
    Username: string;
    Password: string;
    constructor() {
        this.Username = '';
        this.Password = '';
    }
}
class ClientModel {
    PersonaId: number;
    FirstName: string;
    LastName: string;
    Email: string;
    Password: string;
    IsOnboarded: boolean;
    IsSelfOnboarded: boolean;
    IsSuccess: boolean;
    UserName?: string;
    CrmContactId?: number;
    constructor() {
        this.IsOnboarded = false;
        this.IsSelfOnboarded = true;
        this.IsSuccess = false;
    }
}

@Component({
    selector: 'sdUser',
    templateUrl: './sd-user.component.html',
    styleUrls: ['./sd-user.component.less'],
    animations: [fadeInOut]
})

export class SdUserComponent implements OnInit, OnDestroy {
    public environments: EnvironmentModel[];
    public vm: SdCreationModel = {};
    public client: ClientModel = new ClientModel();
    public personasList: Personas[];
    public loading = false;
    constructor(
        private apiEndpointService: ApiEndpointService,
        private alertService: AlertService,
        private foAuthService: FoAuthService,
        private enviromentService: EnvironmentService,
        private loaderService: LoaderService,
        private foPersonaService: FOPersonaService) {
    }

    ngOnInit(): void {
        const self = this;
        self.loaderService.on();
        self.enviromentService.getEnvironments().subscribe(resp => {
            self.environments = resp;
            self.loaderService.off();
        });
    }

    ngOnDestroy(): void {
        const self = this;
        self.foAuthService.endSession();
    }

    // Events
    onSelectEnvironment(): void {
        const self = this;
        self.resetInsitution();
        self.vm.SelectedEnvironment = self.environments.find(e => e.Id === Number(this.vm.EnvironmentId));
    }

    onSelectInstitution(): void {
        const self = this;
        self.resetBranch();
        if (self.vm.SelectedEnvironment) {
            const env = self.vm.SelectedEnvironment.Institution.find(e => e.Id === Number(this.vm.InstitutionId));
            if (env) {
                self.enviromentService.setUpEnvironment(self.vm.SelectedEnvironment, env);
                self.vm.SelectedInstitution = env;
            }
        }
    }

    onSelectBranch(): void {
        const self = this;
        self.personasList = null;
        if (self.vm.SelectedInstitution) {
            self.vm.SelectedBranch = self.vm.SelectedInstitution.Branches.find(b => b.Id === Number(self.vm.BranchId));
            self.processAgentAuthentication(self.vm.SelectedBranch.AgentEmail, self.vm.SelectedBranch.AgentPassword);
        }
    }

    onClickCreate(): void {
        const self = this;
        self.loaderService.on();
        self.client.Email = self.client.Email.toLocaleLowerCase().trim();
        if (!self.vm.SelectedBranch.IsUserNameLogin) {
            self.client.UserName = self.client.Email;
        }
        self.apiEndpointService.callFoApi<IApiResult<string>>(ApiMethod.POST, '/api/agent/users', self.client).subscribe(resp => {
            self.client.IsSuccess = resp.Success;
            if (!resp.Success) {
                self.alertService.show(resp.ErrorMessage);
            }
            self.loaderService.off();
        }, err => {
            self.alertService.show('Could not create user');
            self.loaderService.off();
        });
    }

    onClickReset(form: NgForm): void {
        const self = this;
        self.client = new ClientModel();
        form.form.markAsUntouched();
    }

    onClickUseDefaultPassword(): void {
        const self = this;
        if (self.client) {
            self.client.Password = ApplicationConst.DEFAULT_PASSWORD;
        }
    }

    onToggleCustomAgent(): void {
        const self = this;
        self.resetInsitution();
        if (self.vm.isUseCustomAgent) {
            self.vm.CustomAgent = new CustomAgentModel();
        }
    }
    onClickAuthorizeCustomAgent(): void {
        const self = this;
        if (!self.vm.CustomAgent) { return; }
        self.processAgentAuthentication(self.vm.CustomAgent.Username, self.vm.CustomAgent.Password);
    }

    // helper

    private loadPersonas(): void {
        const self = this;
        self.loaderService.on();
        self.foPersonaService.getPersonas().subscribe(resp => {
            if (resp.Results) {
                self.personasList = resp.Results.Personas;
                self.client.PersonaId = self.personasList[0].Id;
            }
        }, err => {
            self.alertService.show('Fail to fetch default personas');
        }).add(() => {
            self.loaderService.off();
        });
    }

    private processAgentAuthentication(username: string, password: string): void {
        const self = this;
        if (!username || !password) { return; }
        self.loaderService.on();
        self.personasList = null;
        self.foAuthService
            .authorize(username, password)
            .subscribe(isSuccess => {
                if (isSuccess) {
                    self.loadPersonas();
                }
            }, err => {
                self.alertService.show('Authorization Fail');
            }).add(() => {
                self.loaderService.off();
            });
    }
    private resetBranch(): void {
        const self = this;
        self.personasList = null;
        self.vm.BranchId = null;
        self.vm.SelectedBranch = null;
    }

    private resetInsitution(): void {
        const self = this;
        self.resetBranch();
        self.vm.InstitutionId = null;
        self.vm.SelectedInstitution = null;
    }
}

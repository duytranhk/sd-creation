import { Component } from '@angular/core';
import { fadeInOut } from '../../services/animation.service';
import { ConfigurationService } from '../../services/configuration.service';
import { AuthService } from 'src/app/services/auth.service';
import { ElectronService } from 'ngx-electron';
import { AppEvents } from 'electron/core/constants/common.contants';
import { ILoginResponse } from 'electron/core/models/credential.model';
import { LoaderService } from 'src/app/services/loader.service';


@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.less'],
    animations: [fadeInOut]
})
export class LoginComponent {
    constructor(
        readonly configurations: ConfigurationService,
        readonly authService: AuthService,
        readonly electronService: ElectronService,
        readonly loaderService: LoaderService) {
    }

    public onLoginClick(): void {
        const self = this;
        self.loaderService.on();
        const response = self.electronService.ipcRenderer.sendSync(AppEvents.APP_LOGIN);
        if (response.IsSuccess) {
            this.authService.processUserLogin(response.Credential);
        }
        self.loaderService.off();
    }
}

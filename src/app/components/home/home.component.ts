import { Component } from '@angular/core';
import { fadeInOut } from '../../services/animation.service';
import { ConfigurationService } from '../../services/configuration.service';
import { Router } from '@angular/router';


@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.less'],
    animations: [fadeInOut]
})
export class HomeComponent {
    constructor(private configurations: ConfigurationService, private router: Router) {
    }
}

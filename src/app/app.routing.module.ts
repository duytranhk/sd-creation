import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthService } from './services/auth.service';
import { AuthGuardService } from './services/auth-guard.service';
import { HomeComponent } from './components/home/home.component';
import { SdUserComponent } from './components/sd-user/sd-user.component';
import { LoginComponent } from './components/login/login.component';

const publicRoutes: Routes = [
    {
        path: 'signin',
        component: LoginComponent,
        data: { title: 'Login' },
    },
];

const authRoutes: Routes = [
    {
        path: 'home',
        component: HomeComponent,
        canActivate: [AuthGuardService],
        data: { title: 'Users' },
    },
    {
        path: 'sd-user',
        component: SdUserComponent,
        canActivate: [AuthGuardService],
        data: { title: 'SD User' },
    },
];

const otherRoutes: Routes = [
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',
    },
];

@NgModule({
    imports: [RouterModule.forRoot([...publicRoutes, ...authRoutes, ...otherRoutes])],
    exports: [RouterModule],
    providers: [AuthService],
})
export class AppRoutingModule { }

export interface IApiResult<T> {
    ErrorCode: number;
    ErrorMessage: string;
    ErrorSource: string;
    Results: T;
    Success: boolean;
}

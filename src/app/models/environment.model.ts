export class EnvironmentModel {
    Id: number;
    Name: string;
    Code: string;
    BaseUrl: string;
    IsEnable: boolean;
    Institution: InsitutionModel[];
}

export class InsitutionModel {
    Id: number;
    Code: string;
    Name: string;
    Branches: BranchModel[];
}

export class BranchModel {
    Id: number;
    BranchName: string;
    AgentEmail: string;
    AgentPassword: string;
    IsUserNameLogin?: boolean;
}

export interface UserModel {
    Email: string;
    Department: string;
    DisplayName: string;
}

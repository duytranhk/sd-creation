export class StorageKey {
  public static readonly USER_ACCESS_TOKEN = 'user@access_token';
  public static readonly USER_TOKEN_TYPE = 'user@token_type';
  public static readonly USER_TOKEN_EXPIRY = 'user@token_expiry';

  public static readonly EVN_API_URL = 'env@api_url';
  public static readonly EVN_ACCESS_TOKEN = 'env@access_token';
  public static readonly EVN_TOKEN_TYPE = 'env@token_type';
}

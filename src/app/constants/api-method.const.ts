import { Injectable } from '@angular/core';

@Injectable()
export class ApiMethod {
  public static readonly GET = 'GET';
  public static readonly POST = 'POST';
  public static readonly PUT = 'PUT';
  public static readonly DELETE = 'DELETE';
}


import { app, BrowserWindow, ipcMain } from 'electron';
import { Authenticator } from './authenticator';
const { autoUpdater } = require('electron-updater');
import * as path from 'path';
import * as url from 'url';

let mainWindow: BrowserWindow;
class SdCreationApp {
  private authenticator: Authenticator;

  constructor() {
    this.authenticator = new Authenticator();
  }
  // Load up our main window for the web application.

  public initialize() {

    // Create the browser window.
    mainWindow = new BrowserWindow(
      {
        width: 650,
        height: 800,
        minHeight: 700,
        minWidth: 500,
        maxWidth: 700,
        webPreferences: {
          nodeIntegration: true,
          contextIsolation: false,
        },
      }
    );
    mainWindow.removeMenu();
    mainWindow.webContents.openDevTools();

    // and load the index.html of the app.
    mainWindow.loadURL(
      url.format({
        pathname: path.join(__dirname, `/../../dist/sd-creation/index.html`),
        protocol: 'file:',
        slashes: true
      })
    );
    mainWindow.on('closed', () => {
      mainWindow = null;
    });

    // Watch for IPC changes.
    this.authenticator.initIPCWatchers(mainWindow);

    // Auto Update
    autoUpdater.checkForUpdates();
  }
}

autoUpdater.on('update-downloaded', (info) => {
  autoUpdater.autoInstallOnAppQuit = true;
});

app.on('ready', () => {
  const application = new SdCreationApp();
  application.initialize();
});

app.on('activate', () => {
  if (mainWindow === null) {
    const application = new SdCreationApp();
    application.initialize();
  }
});

app.on('certificate-error', (event, webContents, appUrl, error, certificate, callback) => {
  event.preventDefault();
  callback(true);
});


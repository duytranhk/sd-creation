interface IAppEvents {
    APP_LOGIN: string;
    APP_LOGOUT: string;
}
export const AppEvents: IAppEvents = {
    APP_LOGIN: '@app:login',
    APP_LOGOUT: '@app:logout',
};

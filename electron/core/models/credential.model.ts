export interface ILoginResponse {
    IsSuccess: boolean;
    Credential: UserCredential;
}
export class UserCredential {
    constructor(
        public AccessToken: string,
        public TokenType: string,
        public TokenExpiry: number
    ) { }
}


export const ElectronConfig = {
  EXPRESS: {
    protocol: 'localhost',
    port: '44300',
    passphrase: 'Passw0rd!'
  },
  AZURE_AD: {
    ClientID: '5cd4f42b-6fb9-4225-be17-46b7053519a6',
    TenantID: 'b56e2da9-c836-4504-90b2-32b8a9eed92d',
    RedirectURL: 'https://localhost:44300/signin',
    PostLogoutRedirectUri: 'https://localhost:44300/signin',
    AzureGraphEndpoint: 'https://graph.windows.net/',
    AadInstance: 'https://login.microsoftonline.com/',
    Scope: 'openid+profile',
    LoginFailureURL: 'https://login.microsoftonline.com/common/federation/oauth2'
  }
};

import { ipcMain, BrowserWindow, session, IpcMessageEvent } from 'electron';
import * as uuid from 'uuid';
import { ApplicationServer } from './application-server';
import { ElectronConfig } from './configuration';
import { AppEvents } from './core/constants/common.contants';
import { UserCredential } from './core/models/credential.model';

// State and PKCE verifiers.
const authStateIdentifier = Math.random().toString(36).substring(7);

export class Authenticator {
    private expressApp: ApplicationServer;
    private authWindow: BrowserWindow;
    private isForceClose = true;

    public initIPCWatchers(mainWindow) {
        ipcMain.on(AppEvents.APP_LOGIN, (event: IpcMessageEvent) => {
            this.checkAuth(event);
        });

        ipcMain.on('routeToHome', () => {
            mainWindow.loadURL(`file://${__dirname}/index.html`);
        });
    }

    private async checkAuth(event: IpcMessageEvent) {
        await this.initializeAuthWindow(event);
        this.authWindow.show();
    }

    private async initializeAuthWindow(event: IpcMessageEvent): Promise<void> {
        this.expressApp = new ApplicationServer();
        await this.expressApp.start();

        this.authWindow = new BrowserWindow(
            {
                show: false,
                webPreferences: {
                    nodeIntegration: false,
                    contextIsolation: true
                }
            }
        );

        this.authWindow.removeMenu();
        this.authWindow.loadURL(`https://${ElectronConfig.EXPRESS.protocol}:${ElectronConfig.EXPRESS.port}`);

        this.authWindow.on('closed', () => {
            this.authWindow = null;
            if (this.isForceClose) {
                this.processAfterLoginRequest(event, null);
            }
        });

        this.authenticate(event);
    }

    private authenticate(event: IpcMessageEvent): void {
        const authUrl =
            'https://login.microsoftonline.com/' +
            ElectronConfig.AZURE_AD.TenantID +
            '/oauth2/authorize?client_id=' +
            ElectronConfig.AZURE_AD.ClientID +
            '&redirect_uri=' +
            encodeURI(ElectronConfig.AZURE_AD.RedirectURL) +
            '&resource=' + encodeURI(ElectronConfig.AZURE_AD.AzureGraphEndpoint) +
            '&response_type=id_token+token' +
            '&scope=' + ElectronConfig.AZURE_AD.Scope +
            '&state=' + authStateIdentifier +
            '&nonce=' + uuid();

        // handle login success
        session.defaultSession.webRequest.onCompleted(
            {
                urls: [ElectronConfig.AZURE_AD.RedirectURL, ElectronConfig.AZURE_AD.LoginFailureURL]
            }, details => {
                const url = details.url.split('#')[1];
                const params = new URLSearchParams(url);
                const accessToken = params.get('access_token');
                const tokenType = params.get('token_type');
                const tokenExpiry = params.get('expires_in');
                const state = params.get('state');
                const credential = new UserCredential(accessToken, tokenType, Number(tokenExpiry));

                // Ensure our original State identifier that we created matches the returned state value in the response.
                if (accessToken && state === authStateIdentifier) {
                    if (accessToken) {
                        this.authWindow.loadURL(`https://${ElectronConfig.EXPRESS.protocol}:${ElectronConfig.EXPRESS.port}`);
                    }
                }
                this.isForceClose = false;
                this.processAfterLoginRequest(event, credential);
            });

        this.authWindow.loadURL(authUrl);
    }

    private processAfterLoginRequest(event: IpcMessageEvent, credential: UserCredential): void {
        event.returnValue = {
            IsSuccess: credential && credential.AccessToken,
            Credential: credential
        };
        if (this.authWindow) {
            this.authWindow.close();
        }
        this.expressApp.stop();
    }
}

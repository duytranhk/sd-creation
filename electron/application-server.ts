import * as express from 'express';
import * as https from 'https';
import * as fs from 'fs';
import * as path from 'path';
import { ElectronConfig } from './configuration';

export class ApplicationServer {
    private app;
    private appServer;

    constructor() {
        this.app = express();
    }

    public start(): Promise<null> {
        return new Promise(resolve => {
            this.app.get('*', (req, res) => res.send('Handsome Duy!'));
            this.appServer = https.createServer({
                key: fs.readFileSync(path.join(__dirname, '../certificate/key.pem')),
                cert: fs.readFileSync(path.join(__dirname, '../certificate/cert.pem')),
                passphrase: ElectronConfig.EXPRESS.passphrase
            }, this.app).listen(ElectronConfig.EXPRESS.port, () => {
                console.log(`\nExpress app listening on port ${ElectronConfig.EXPRESS.port}!\n`);
                return resolve();
            });
        });
    }

    public stop() {
        console.log(`\nExpress app closing on port ${ElectronConfig.EXPRESS.port}!\n`);
        this.appServer.close();
    }
}

# SD Creation
This project is powered by BTO Tiger team, for internal using
Built on top of Angular 8 and Electron

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory.
Run `npm run start` to run the project.

## Further help

To get more help, please contact BTO Tiger Team
